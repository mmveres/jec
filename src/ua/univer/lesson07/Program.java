package ua.univer.lesson07;

import java.sql.SQLOutput;
import java.util.*;

enum VagonEnum{
    CARGO(0), PASSENGER(1);

    private int n;

    VagonEnum(int n) {

        this.n = n;
    }

    @Override
    public String toString() {
        return this.name() +
                "{n=" + n +
                "} ";
    }
}
interface ITrainStack<E extends Comparable<E> > extends Iterable<E>{
    boolean isEmpty();
    E peek();
    E pop();
    boolean add(E data);
    void addAll(List<E> list);
}
class TrainStack<E extends Comparable<E>> extends AbstractCollection<E> implements ITrainStack<E>{
    private Stack<E> train= new Stack<>();
    @Override
    public boolean isEmpty() {
        return train.isEmpty();
    }

    @Override
    public E peek() {
        return train.peek();
    }

    @Override
    public E pop() {
        return train.pop();
    }

    @Override
    public boolean add(E data) {
      return train.add(data);
    }

    @Override
    public void addAll(List<E> list) {
        train.addAll(list);
    }

    @Override
    public Iterator<E> iterator() {
        return train.iterator();
    }

    @Override
    public int size() {
        return train.size();
    }
}
class TrainStackList<E extends Comparable<E>> implements ITrainStack<E>{
    private List<E> train= new LinkedList<>();
    @Override
    public boolean isEmpty() {
        return train.isEmpty();
    }

    @Override
    public E peek() {
        return train.get(train.size()-1);
    }

    @Override
    public E pop() {
        return train.remove(train.size()-1);
    }

    @Override
    public boolean add(E data) {
        return train.add(data);

    }

    @Override
    public void addAll(List<E> list) {
        train.addAll(list);
    }

    @Override
    public Iterator<E> iterator() {
        return new TrainIterator();
    }

    private class TrainIterator implements Iterator<E> {
        private int i;

        public TrainIterator() {
            this.i = -1;
        }

        @Override
        public boolean hasNext() {
            return i < train.size()-1;
        }

        @Override
        public E next() {
            return train.get(++i);
        }
    }
}
public class Program {
    public static void main(String[] args) {
        ITrainStack<VagonEnum> train= new TrainStack<>();
        ITrainStack<VagonEnum> passTrain= new TrainStackList<>();
        ITrainStack<VagonEnum> cargoTrain= new TrainStack<>();

        train.addAll(Arrays.asList(
                VagonEnum.CARGO,
                VagonEnum.CARGO,
                VagonEnum.PASSENGER,
                VagonEnum.PASSENGER,
                VagonEnum.CARGO
        ));
        train.add(VagonEnum.CARGO);

        System.out.println("******************");
        for (VagonEnum vagon: train) {
            System.out.println(vagon);
        }
        while(!train.isEmpty()){
            if (train.peek() == VagonEnum.CARGO)
                cargoTrain.add(train.pop());
            else
                passTrain.add(train.pop());
        }
        System.out.println("******************");
        for (VagonEnum vagon: cargoTrain) {
            System.out.println(vagon);
        }
        System.out.println("******************");
        for (VagonEnum vagon: passTrain) {
            System.out.println(vagon);
        }
    }
}
