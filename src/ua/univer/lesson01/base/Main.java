package ua.univer.lesson01.base;

import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException {
        System.out.print("Hi\n");
        System.err.println(args[0]);
        System.err.println(args[1]);
        System.out.println(Arrays.toString(args));
        Scanner sc = new Scanner(System.in);
        String str = sc.next();
        System.out.print("b ="+str);

    }
}
