package ua.univer.lesson02.base.figures;

import ua.univer.lesson02.base.figures.modelfigure.*;

public class ProgramFigure {
    public static void main(String[] args) {
        Point p = new Point(2, 3);
        System.out.println(p);
        Point p1 = new Point();
        ColorPoint cp = new ColorPoint(2, 3, "Red");
        Line line1 = new Line(p, p1);
        Line line2 = new Line(2, 2, 3, 3);
        Line line3 = new Line(new Point(4, 4), new Point(5, 5));
        Point p4b = line2.getBeg();
        Point p4e = line2.getEnd();
        ColorLine cline1 = new ColorLine(p4b, p4e, "Green");
        Triangle tri = new Triangle(p1, p4b, p4e);

        int[] mas = {1, 2, 3, 4};
        AbstractFigure[] masFigures = {
                p1,
                p4b,
                line1,
                cline1,
                tri
        };
        for (int i = 0; i < masFigures.length; i++) {
            System.out.println(masFigures[i]);
        }

        for (Object obj : masFigures) {
                System.out.println(obj);
        }
    }
}
