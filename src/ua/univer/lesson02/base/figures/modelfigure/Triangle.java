package ua.univer.lesson02.base.figures.modelfigure;

public class Triangle extends AbstractFigure{
    private Point A;
    private Point B;
    private Point C;
    private Line sideAB;
    private Line sideBC;
    private Line sideAC;


    public Triangle(Point a, Point b, Point c) {
        A = a;
        B = b;
        C = c;
       // sideAB = new Line(A,B);
       // sideBC = new Line(B,C);
       // sideAC = new Line(A,C);
    }
    public Line getSideAB(){
        if (sideAB==null) sideAB = new Line(A,B);
        return sideAB;
    }
    public Line getSideAC(){
        if (sideAC==null) sideAC = new Line(A,C);
        return sideAC;
    }
    public Point getA() {
        return A;
    }

    public void setA(Point a) {
        sideAB = null;
        sideAC = null;
        A = a;
    }

    public Point getB() {
        return B;
    }

    public void setB(Point b) {
        B = b;
    }

    public Point getC() {
        return C;
    }

    public void setC(Point c) {
        C = c;
    }

    @Override
    public String toString() {
        return "Triangle{" +
                "A=" + A +
                ", B=" + B +
                ", C=" + C +
                super.toString()+'}';
    }
}
