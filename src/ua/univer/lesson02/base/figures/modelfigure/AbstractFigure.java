package ua.univer.lesson02.base.figures.modelfigure;

public abstract class AbstractFigure {
    private static int globalId;
    private int currentId;

    public AbstractFigure() {
        this.currentId = globalId++;
    }

    @Override
    public String toString() {
        return "currentId=" + currentId;
    }
}
