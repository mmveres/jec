package ua.univer.lesson02.base.figures.modelfigure;

public class ColorLine extends Line{
    private String color;

    public ColorLine(Point beg, Point end, String color) {
        super(beg, end);
        this.color = color;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "ColorLine{" +
                "color='" + color + '\'' +
                "} " + super.toString();
    }
}
