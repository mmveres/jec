package ua.univer.lesson05.example;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Ex4 {

	public static void main(String[] args) {
		String text = "Test text for checking the work with regular expressions. The task is to"
				+ "find in the text all the words that read and end with the same letter,"
				+ "for example, ABABAHALAMAHA.";
		Pattern p;

		p = Pattern.compile("\\b(\\w)\\w+\\1\\b");
		Matcher m = p.matcher(text);
		while (m.find()) {
			System.out.println(m.group());
		}
	}

}
