package ua.univer.lesson05.example;

import java.io.UnsupportedEncodingException;

public class Ex3 {

	public static void main(String[] args) throws UnsupportedEncodingException {

		String str1 = "Тестування перекодування символів";
		System.out.println(str1);

		byte[] b = str1.getBytes("Cp1251");
		
		String str2 = new String(b, "KOI8-R");
		System.out.println(str2);
		
		String str3 = new String(b, "Cp866");
		System.out.println(str3);

		String str4 = new String(b, "ISO8859-5");
		System.out.println(str4);
	}
	
	


}
