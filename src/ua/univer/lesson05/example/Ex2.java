package ua.univer.lesson05.example;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.Locale;
import java.util.ResourceBundle;

public class Ex2 {

	public static void main(String[] args) throws ParseException {
		Date date = new Date();
		DateFormat df;
		Locale locale;
		String str;
		Date parseDate;
		NumberFormat nf;
		double value = 12345.6789;
		Number d;

		locale = Locale.getDefault();
		locale = new Locale("ua", "UA");
		//locale = new Locale("en", "EN");
		System.out.println(locale);

		ResourceBundle bundle;
//		bundle = ResourceBundle.getBundle("resources.prop");
		bundle = ResourceBundle.getBundle("Prop", locale);
		System.out.println(bundle.getString("main.name"));

		nf = NumberFormat.getNumberInstance();
		System.out.println(nf.format(value));
		nf = NumberFormat.getCurrencyInstance();
		System.out.println(nf.format(value));
		d = nf.parse("$1234.567");
		System.out.println(d);

		df = DateFormat.getDateInstance(DateFormat.SHORT);
		System.out.println(df.format(date));
		df = DateFormat.getDateInstance(DateFormat.MEDIUM);
		System.out.println(df.format(date));
		df = DateFormat.getDateInstance(DateFormat.LONG);
		System.out.println(df.format(date));
		df = DateFormat.getDateInstance(DateFormat.FULL);
		System.out.println(df.format(date));

		str = df.format(date);
		parseDate = df.parse(str);
		System.out.println(parseDate);
		System.out.println();

		locale = new Locale("uk", "UA");

		System.out.println(locale);

		nf = NumberFormat.getNumberInstance(locale);
		System.out.println(nf.format(value));
		nf = NumberFormat.getCurrencyInstance(locale);
		System.out.println(nf.format(value));

		d = nf.parse(nf.format(value));
		System.out.println(d);

		df = DateFormat.getDateInstance(DateFormat.SHORT, locale);
		System.out.println(df.format(date));
		df = DateFormat.getDateInstance(DateFormat.MEDIUM, locale);
		System.out.println(df.format(date));
		df = DateFormat.getDateInstance(DateFormat.LONG, locale);
		System.out.println(df.format(date));
		df = DateFormat.getDateInstance(DateFormat.FULL, locale);
		System.out.println(df.format(date));
		str = df.format(date);
		parseDate = df.parse(str);
		System.out.println(parseDate);
		System.out.println();
	}
}
