package ua.univer.lesson05.example;

public class Ex1 {

	public static void main(String[] args) {

		String s1 = "abc";
		String s2 = new String("abcddddddddd");

		System.out.println(s1 == s2.intern());
		System.out.println(s1.equals(s2));
		System.out.println(s1.compareTo(s2));

		StringBuffer sb = new StringBuffer("abc");
		sb = sb.append("12345123451234512");
		System.out.println(sb.length() + " " + sb.capacity());

		s1 = "123";
		s2 = new String(s1);

		System.out.println(System.identityHashCode(s1));
		System.out.println(System.identityHashCode(s2));
		System.out.println(s1.hashCode());

		s2 = s2.intern();
		System.out.println(System.identityHashCode(s1));
		System.out.println(System.identityHashCode(s2));

	}

}
