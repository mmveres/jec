package ua.univer.lesson05;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.time.LocalDateTime;
import java.util.*;

// https://www.stihi-rus.ru/World/Shekspir/1.htm
// https://data.gov.ua/dataset/a2f5bac5-5507-4da9-a9d7-ffb45349c270
// https://stackoverflow.com/questions/4328711/read-url-to-string-in-few-lines-of-java-code
class KmdaUser{
    private String name;
    private double oklad;

    public KmdaUser(String name, double oklad) {
        this.name = name;
        this.oklad = oklad;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getOklad() {
        return oklad;
    }

    public void setOklad(double oklad) {
        this.oklad = oklad;
    }

    @Override
    public String toString() {
        return "KmdaUser{" +
                "name='" + name + '\'' +
                ", oklad=" + oklad +
                '}';
    }
}
public class ProgramString {
    public static void main(String[] args) throws IOException {
      //  String filename ="C:"+ File.separator+"in"+File.separator+"sonet"+File.separator+"zp-lupen-2019.csv";
      //  BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(filename), "CP1251"));

        String url = "https://data.gov.ua/dataset/770cc750-4333-424f-b6e9-6e6c5c76aec9/resource/59cb6066-1fac-41ed-a571-811db551c75b/download/zp-lupen-2019.csv";
        URL website = new URL(url);
        URLConnection connection = website.openConnection();
        BufferedReader br = new BufferedReader(
                new InputStreamReader(
                        connection.getInputStream(),"CP1251"));

        List<KmdaUser> users = getKmdaByStringBuilder(br);
        System.out.println(users);
        for (KmdaUser user: users) {
            System.out.println(user);
        }
        KmdaUser max_oklad_user = users.get(0);
        for (KmdaUser user: users) {
           if (user.getOklad()> max_oklad_user.getOklad())
               max_oklad_user = user;
        }
        System.out.println("max oklad = "+max_oklad_user);
    }

    private static void testSpeedString(String filename) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(filename));
        String allStr1 = getConCatByString(br);
        //System.out.println(allStr1);
        br = new BufferedReader(new FileReader(filename));
        String allStr2 = getConCatByStringBuilder(br);
        br = new BufferedReader(new FileReader(filename));
        String allStr3 = getConCatByStringBuffer(br);
    }

    private static List<KmdaUser> getKmdaByStringBuilder(BufferedReader br) throws IOException {
        List<KmdaUser> users = new ArrayList<>();
        String titleStr = br.readLine();
        String [] titles = titleStr.split(";");

        while(br.ready()){
           String[] usersInfo = br.readLine().split(";");
           users.add(new KmdaUser(usersInfo[0], Double.parseDouble(usersInfo[2].replace(',','.'))));
        }
        return users;
    }

    private static String getConCatByStringBuilder(BufferedReader br) throws IOException {
        StringBuilder sbuild = new StringBuilder(1000);
        long start = LocalDateTime.now().getNano();
        System.out.println(start);
        System.out.println(sbuild.length()+"/"+sbuild.capacity());
        while(br.ready()){
            sbuild.append(br.readLine()+"\n");
      //      System.out.println(sbuild.length()+"/"+sbuild.capacity());
        }
        long end = LocalDateTime.now().getNano();
        System.out.println(end);
        System.out.println(end-start);
        return sbuild.toString();
    }

    private static String getConCatByStringBuffer(BufferedReader br) throws IOException {
        StringBuffer sbuild = new StringBuffer();
        long start = LocalDateTime.now().getNano();
        System.out.println(start);
        System.out.println(sbuild.length()+"/"+sbuild.capacity());
        while(br.ready()){
            sbuild.append(br.readLine()+"\n");
      //      System.out.println(sbuild.length()+"/"+sbuild.capacity());
        }
        long end = LocalDateTime.now().getNano();
        System.out.println(end);
        System.out.println(end-start);
        return sbuild.toString();
    }

    private static String getConCatByString( BufferedReader br) throws IOException {
        String allStr ="";
        long start = LocalDateTime.now().getNano();
        System.out.println(start);
        while(br.ready()){
            allStr += br.readLine()+"\n";
     //       System.out.println(allStr.length());
        }
        long end = LocalDateTime.now().getNano();
        System.out.println(end);
        System.out.println(end-start);
        return allStr;
    }

    private static void readFromFileByScanner(String filename) throws FileNotFoundException {
        Scanner sc = new Scanner(new FileInputStream(filename));
        while(sc.hasNextLine()) {
            String word = sc.nextLine();
            System.out.println(word);
        }
    }


    private static void testConsoleScanner() {
        Scanner sc = new Scanner(System.in);
        while(sc.hasNext()) {
            String word = sc.next();
            System.out.println(word);
        }
    }

    private static void testSplit() {
        String str= "mama mila ramu ramu mila mama mama";
        String [] words = str.split(" ");
        System.out.println(Arrays.toString(words));
        System.out.println(words.length);
    }

    private static void testReplace() {
        String value1 = " 2,14 ";
        String value2 = " 3,14 ";
        double v1 = Double.parseDouble(value1.replace(',','.').trim());
        double v2 = Double.parseDouble(value2.replace(',','.').trim());
        double res = v1+v2;
        System.out.println(res);
    }

    private static void findCountWordInText() {
        String str= "mama mila ramu ramu mila mama mama";
        String word = "mama";
        int index = str.indexOf(word);
        System.out.println(index);
        int lastIndex =  str.lastIndexOf(word);
        while (index < lastIndex){
            index = str.indexOf(word,index+1);
            System.out.println(index);
        }
    }

    private static void testStr1() {
        String str = "Cat";
        String str1 = "Cat";
        String str2 = "Cat";
        String str3 = "Cat";
        System.out.println(str == str1 && str1 == str2 && str2 == str3);
        str +="1";
        System.out.println(str);
    }
}
