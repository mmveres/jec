package ua.univer.lesson03;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Objects;

class Ship implements Comparable<Ship>{
    private int id;
    private String name;
    private String port;
    private int x;
    private int y;

    public Ship(int id, String name, String port, int x, int y) {
        this.id = id;
        this.name = name;
        this.port = port;
        this.x = x;
        this.y = y;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Ship ship = (Ship) o;
        return id == ship.id && name.equals(ship.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }

    @Override
    public String toString() {
        return "Ship{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", port='" + port + '\'' +
                ", x=" + x +
                ", y=" + y +
                '}';
    }


    @Override
    public int compareTo(Ship o) {
        return getX() - o.getX() ;
    }
}
class CompareById implements Comparator<Ship> {

    @Override
    public int compare(Ship o1, Ship o2) {
        return o1.getId()- o2.getId();
    }
}
class CompareByName implements Comparator<Ship>{

    @Override
    public int compare(Ship o1, Ship o2) {
        return o1.getName().compareTo(o2.getName());
    }
}
public class Program {
    public static void main(String[] args) {
       // testEquals();
        Ship ship1 = new Ship(1,"Ship1","Odessa",1,1);
        Ship ship1move = new Ship(1,"Ship1","Odessa",2,3);
        Ship ship1ill = new Ship(1,"Ship1","Illichonsk",20,30);
        System.out.println(ship1 == ship1move);
        System.out.println(ship1.equals(ship1move));
        System.out.println(ship1.equals(ship1ill));
        System.out.println(ship1.hashCode());
        System.out.println(ship1ill.hashCode());


        int [] mas = {1,2,13,4,5,2};
        Arrays.sort(mas);
        System.out.println(Arrays.toString(mas));
        Ship [] masShip = {ship1, ship1move, ship1ill, new Ship(2,"Ship2","Odessa2",3,4)};
        Arrays.sort(masShip, (o1, o2) -> o1.getY()-(o2.getY()));
        for (Ship ship : masShip) {
            System.out.println(ship);
        }

    }


    private static void testEquals() {
        Object obj1 = new Object();
        Object obj2 = new Object();
        System.out.println(obj1 == obj2);
        System.out.println(obj1.hashCode());
        System.out.println(obj2.hashCode());
        System.out.println(obj1);
        System.out.println(obj2);
        System.out.println(obj1.equals(obj2));
        System.out.println("************************");
        String str1 = "A";
        String str2 = "A";
        String str3 = new String("A");
        String str4 = new String("A");
        System.out.println(str1==str2);
        System.out.println(str3==str4);
        System.out.println(str3.equals(str4));
    }
}
