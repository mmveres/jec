package ua.univer.lesson03.multyInterface;

interface A {
    int value = 3;
 //   int getValue();
}
interface B {
    double value = 5.4;
 //   double getValue();
}
public class C implements A, B {
    public static void main (String s []) {
        C c = new C ();
        // System.out.println (c.value); - mistake!
        System.out.println (((A) c) .value);
        System.out.println (((B) c) .value);

    }

//    @Override
//    public int getValue() { - error
//        return 0;
//    }
}
