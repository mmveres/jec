package ua.univer.lesson08;

import java.util.*;
class Ship{
    private String name;
    private int number;
    private int x;
    private int y;

    public Ship(String name, int number, int x, int y) {
        this.name = name;
        this.number = number;
        this.x = x;
        this.y = y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Ship ship = (Ship) o;
        return number == ship.number &&  name.equals(ship.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, number);
    }

    @Override
    public String toString() {
        return "Ship{" +
                "name='" + name + '\'' +
                ", number=" + number +
                ", x=" + x +
                ", y=" + y +
                '}';
    }
}
public class Program {
    public static void main(String[] args) {
        Set<Object> listObj = new HashSet<>();
        listObj.add(new Object());
        listObj.add(new Object());
        listObj.add(1);
        listObj.add(1.0);
        listObj.add(1.0f);
        listObj.add(1l);
        listObj.add("Tom");
        listObj.add(1);
        listObj.add(new Ship("Odessa",111, 1,1));
        listObj.add(new Ship("Odessa",111, 2,2));

        System.out.println(listObj);
        for (Object elem: listObj) {
            System.out.println(elem.getClass()+" = "+elem.hashCode());
        }
        System.out.println(listObj.contains(new Ship("Odessa",111, 23,41)));


        List<Ship> shipsRoute= new ArrayList();
        shipsRoute.add(new Ship("Odessa",111, 1,1) );
        shipsRoute.add(new Ship("Odessa",111, 2,2) );
        shipsRoute.add(new Ship("Odessa",111, 4,5) );
        shipsRoute.add(new Ship("Tavrida",222, 2,1) );
        shipsRoute.add(new Ship("Tavrida",222, 2,2) );

        Set<Ship> ships = new HashSet<>();
        ships.addAll(shipsRoute);
        System.out.println(ships);


    //    getMapSport();
    String str = "mama mila ramu ramu mila mama mama";
    String [] words = str.split(" ");

        Map<String, Integer> mapFreg1 = getFreqByIf(words);
        System.out.println(mapFreg1);

        Map<String, Integer> mapFreg2 = getFreqByTerna(words);
        System.out.println(mapFreg2);

    }

    private static Map<String, Integer> getFreqByTerna(String[] words) {
        Map<String, Integer> mapFreg = new HashMap<>();
        for (String word : words) {
            Integer freq = mapFreg.get(word);
            mapFreg.put(word,(freq == null)? 1: freq+1);
        }
        return mapFreg;
    }

    private static Map<String, Integer> getFreqByIf(String[] words) {
        Map<String, Integer> mapFreg = new HashMap<>();
        for (int i = 0; i < words.length; i++) {
            if(mapFreg.get(words[i])==null)
                mapFreg.put(words[i],1);
            else mapFreg.put(words[i], mapFreg.get(words[i])+1);
        }
        return mapFreg;
    }

    private static Map<String, List<Integer>> getMapSport() {
        Map<String, List<Integer>> sportmens= new HashMap<>();

        if (!sportmens.containsKey("Tom")) sportmens.put("Tom",new ArrayList<>());

        sportmens.get("Tom").add(9);
        sportmens.get("Tom").add(19);
        sportmens.get("Tom").add(92);
        if (!sportmens.containsKey("Bob")) sportmens.put("Bob",new ArrayList<>());
        sportmens.get("Bob").add(92);

        System.out.println(sportmens);
        return sportmens;
    }


}
