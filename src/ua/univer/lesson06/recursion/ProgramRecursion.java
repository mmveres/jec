package ua.univer.lesson06.recursion;
public class ProgramRecursion {
    static int count;
    static{
        count = 0;
    }

    // n! = n*(n-1)!
    public static int factRec(int n){
        System.out.println(++count);
        if (n == 0) return 1;
        if (n == 1) return 1;
        return n* factRec(n-1);
    }

    public static int factIter(int n){
        if (n == 0 || n == 1) return 1;
        int f = 1;
        for (int i = 2; i <= n; i++) {
            f*=i;
        }
        return f;
    }

    //fib(n) = fib(n-1) + fib(n-2)
    public static int fibRec(int n){
        System.out.println(++count);
        if (n == 0) return 0;
        if (n == 1) return 1;
        return fibRec(n-2)+ fibRec(n-1);
    }

    public static int fibIter(int n){
        if (n == 0) return 0;
        if (n == 1) return 1;
        int z = 1;
        int y = 1;
        int x = 0;
        for (int i = 2; i <=n ; i++) {
            z = y+x;
            x = y;
            y = z;
        }
        return z;
    }

    public static void main(String[] args) {
      //  System.out.println(factRec(5));
      //  System.out.println(factIter(5));
        System.out.println(fibRec(10));
        System.out.println(fibIter(10));
    }
}
