package ua.univer.lesson06;

import java.util.ArrayList;
import java.util.HashMap;

class TwoTuple<A,B>{
    public final A first;
    public final B second;
    public TwoTuple(A first, B second){
        this.first = first;
        this.second = second;
    }

    @Override
    public String toString() {
        return "TwoTuple{" +
                "first=" + first.getClass() +
                ", second=" + second.getClass() +
                '}';
    }
}
class Inspector {
    public <T> void inspect(T t) {
        System.out.println(t.getClass());
    }
}
@FunctionalInterface
interface IPrint{
    void print();
}

public class ProgramGen {
    public static void main(String[] args) {
        System.out.println(new TwoTuple<String, String>("1","hi"));
        Inspector i = new Inspector();
        String s = "Hello";
        i.inspect(s);
        i.inspect(1);
        i.inspect(1.0);
        i.inspect(new Object());
        i.inspect( (IPrint) () -> System.out.println("print1"));
        i.inspect(new IPrint() {
            @Override
            public void print() {
                System.out.println("print2");
            }
        });
        i.inspect(new Inspector(){
            @Override
            public <T> void inspect(T t) {

            }
        });
        i.<String>inspect(s);

    }
}
