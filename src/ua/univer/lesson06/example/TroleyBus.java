package ua.univer.lesson06.example;

public class TroleyBus implements Comparable<TroleyBus>{

    private int route;

    public TroleyBus( int route) {

        this.route = route;
    }



    public int getRoute() {
        return route;
    }

    public void setRoute(int route) {
        this.route = route;
    }

    @Override
    public String toString() {
        return "TroleyBus{" +
                "route=" + route +
                '}';
    }

    @Override
    public int compareTo(TroleyBus o) {
        return route-o.getRoute();
    }
}
