package ua.univer.lesson06.example;

public class Bus implements Comparable<Bus>{
    private int number;
    private int route;

    public Bus(int number, int route) {
        this.number = number;
        this.route = route;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getRoute() {
        return route;
    }

    public void setRoute(int route) {
        this.route = route;
    }

    @Override
    public String toString() {
        return "Bus{" +
                "number=" + number +
                ", route=" + route +
                '}';
    }

    @Override
    public int compareTo(Bus o) {
        return route-o.getRoute();
    }
}
