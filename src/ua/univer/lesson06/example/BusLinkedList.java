package ua.univer.lesson06.example;

public class BusLinkedList<T extends Comparable<T>> implements Comparable<BusLinkedList<T>>{
    @Override
    public int compareTo(BusLinkedList<T> other) {
        if (other == null) return 1;
        NodeList iterThis = head;
        NodeList iterOther = other.head;
        while (iterThis != null && iterOther!=null){
            if (iterThis.data.compareTo(iterOther.data)==0){
                iterThis = iterThis.next;
                iterOther = iterOther.next;}
            else return (iterThis.data).compareTo(iterOther.data);
        }
        if (iterThis != null) return 1;
        if (iterOther!= null) return -1;
        return 0;
    }

    private class NodeList{
        T data;
        NodeList next;

        public NodeList(T data) {
            this.data = data;
            this.next = null;
        }
    }
    private NodeList head;

    public BusLinkedList() {
        this.head = null;
    }



    public void addFirst(T data){
        NodeList node = new NodeList(data);
        if (head == null){
            head = node;
        }
        else{
            node.next = head;
            head=node;
        }
    }
    public T removeFirst(){
        if (head!=null){
        T data = head.data;
        head = head.next;
        return data;
        }
        else return null;
    }

    void print(){
        System.out.println("**********************");
        NodeList current = head;
        while(current!=null){
            System.out.println(current.data);
            current = current.next;
        }
    }
}
