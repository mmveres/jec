package ua.univer.lesson06.example;

import java.util.Arrays;
import java.util.Collections;

public class ProgramAuto {
    public static void main(String[] args) {
        BusLinkedList park = new BusLinkedList();
        BusLinkedList route = new BusLinkedList();
        park.addFirst(new Bus(1,11));
        park.addFirst(new Bus(2,11));
        park.addFirst(new Bus(3,22));
        park.addFirst(new Bus(4,22));
        park.addFirst(new Bus(5,11));
        park.addFirst(new TroleyBus(11));
        park.print();
        route.addFirst(park.removeFirst());
        route.addFirst(park.removeFirst());
        park.print();
        route.print();
        int [] mas = {1,2,3,4};
        park.compareTo(route);

    }
}
