package ua.univer.lesson09;

public class NotMarkException extends Exception {
    public NotMarkException(String message) {
        super(message);
    }
}
