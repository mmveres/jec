package ua.univer.lesson09;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class ProgramEx {
    static Logger logger = Logger.getLogger(ProgramEx.class.getName());
    static {
        FileHandler fh = null;
        try {
            fh = new FileHandler("ProgramEx.log",true);
        } catch (IOException e) {
            e.printStackTrace();
        }
      //  fh.setFormatter(new SimpleFormatter());
        logger.setLevel(Level.WARNING);
        logger.addHandler(fh);
    }


    public static void main(String[] args) {
        int value = getIntMarkFromConsole();
        System.out.println(value);
    }



    private static int getIntMarkFromConsole() {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            try {
                System.out.println("enter in value");
                String valueStr = br.readLine();
                logger.info("Entered from console " + valueStr);
                int valueInt = Integer.parseInt(valueStr);
                int mark = getMark(valueInt);
                return mark;
            } catch (NumberFormatException | NotMarkException e) {
                logger.severe(e.getMessage());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private static int getMark(int valueInt) throws NotMarkException {
        logger.info("value by arg"+valueInt);
        if (valueInt<2 || valueInt>12)throw new NotMarkException("Not mark");
        return valueInt;
    }
}
